package com.tabeeb.controller;

import com.tabeeb.entity.Patient;
import com.tabeeb.service.PatientService;
import com.tabeeb.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class PatientController {
    @Autowired
    private PatientService patientService;

    @PostMapping("/patient/register")
    public ResponseEntity<Void> registerUser(@RequestBody @Valid Patient patientUser) {
        patientService.createPatientUser(patientUser);
        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }

    @GetMapping("/patient")
    public ResponseEntity<List<Patient>> getAllPatients() {
        return ResponseEntity.ok(patientService.fetchAllPatients());
    }


    @GetMapping("/patient/{uuid}")
    public ResponseEntity<Patient> getPatient(@PathVariable("uuid") String uuid) {
        return ResponseEntity.ok(patientService.fetchPatient(uuid));
    }

    @GetMapping("/patient/{username}/username")
    public ResponseEntity<Patient> getPatientByUsername(@PathVariable("username") String username) {
        return ResponseEntity.ok(patientService.fetchPatientByUsername(username));
    }


    @PutMapping("/patient/{uuid}/diagnose")
    public ResponseEntity<Void> addDiagnose(@PathVariable("uuid") String uuid,
                                              @RequestParam("diagnose") String diagnose,
                                              @RequestParam("doctor-uuid") String doctorUuid) {
        patientService.addDiagnose(uuid, doctorUuid, diagnose);

        return ResponseEntity.status(HttpStatus.CREATED).body(null);
    }

    @GetMapping("/patient/doctor/{doctor-uuid}")
    public ResponseEntity<List<Patient>> getDoctorPatients(@PathVariable("doctor-uuid") String doctorUuid) {
        return ResponseEntity.ok(patientService.fetchPatientsByDoctor(doctorUuid));
    }

}
